package modelo;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;

import javax.enterprise.context.Dependent;
import javax.inject.Named;

@Named
@Dependent
public class Opinion implements Serializable {
	private static final long serialVersionUID = 1L;
	private int id;
	private int idProduto;
	private Usuario usuario;
	private int valoracion=1; /// Type ENUM ¿?¿ // por defecto vale 1
	private String texto;
	private LocalDateTime data;
	
	public Opinion() {
		super();
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getIdProduto() {
		return idProduto;
	}
	public void setIdProduto(int idProduto) {
		this.idProduto = idProduto;
	}

	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	public int getValoracion() {
		return valoracion;
	}
	public void setValoracion(int valoracion) {
		this.valoracion = valoracion;
	}
	public String getTexto() {
		return texto;
	}
	public void setTexto(String texto) {
		this.texto = texto;
	}
	
	public LocalDateTime getData() {
		return data;
	}
	public void setData(LocalDateTime data) {
		this.data = data;
	}
	public String getTempoPasado() {
		String tempoPasado="";
		LocalDateTime agora = LocalDateTime.now();
		Period p = Period.between(data.toLocalDate(), agora.toLocalDate());
		System.out.println("años; "+p.getYears()+" meses; "+ p.getMonths()+" dias: "+p.getDays());
		tempoPasado +=(p.getYears()>0 || p.getMonths()>0 || p.getDays()>0) ? "fai ": "";
		tempoPasado += (p.getYears()>0) ? p.getYears()+ "ano" : "";
		tempoPasado += (p.getYears()>1) ? "s " : "";
		tempoPasado += (p.getYears()>0 && p.getMonths()>0) ? ", " : "";
		tempoPasado += (p.getMonths()>0) ? p.getMonths() + " mes" : "";
		tempoPasado += (p.getMonths()>1) ? "es" : "";
		tempoPasado += (p.getMonths()>0 || p.getYears()>0) ? " e ": "";
		tempoPasado += (p.getDays()>0 ) ? p.getDays()+" dia" : "";
		tempoPasado += (p.getDays()>1) ? "s" : "";
		tempoPasado += (p.getDays()==0 && p.getMonths()==0 && p.getYears()==0) ? "fai menos dun día": "";
		return tempoPasado;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Opinion other = (Opinion) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	

}
